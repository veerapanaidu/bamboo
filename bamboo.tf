

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8085"
        to_port     = "8085"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "jbos" {
    ami                         =  var.ami
    instance_type               = "t2.large"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.jbos.public_ip
    }

     provisioner "remote-exec" {
    inline = [
"sudo apt update -y",
"sudo  apt install openjdk-8-jre -y",
"sudo install postgresql",
"sudo useradd --create-home -c 'Bamboo role account' bamboo",
"sudo wget https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-5.10.3.tar.gz",
"sudo tar -xvf atlassian-bamboo-5.10.3.tar.gz",
"#sudo ln -s atlassian-bamboo-5.10.3 current",
"#sudo mkdir -p /var/atlassian/application/bamboo",
"#sudo chown bamboo: /var/atlassian/application/bamboo/",
"sudo atlassian-bamboo-5.10.3/bin/start-bamboo.sh",


    ]
  }
   tags = {
    Name = "bamboo"
  }
}